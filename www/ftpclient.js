var exec = require("cordova/exec");
function FtpClient(){};
    
/**
 * Upload a file to a FTP server
 *
 * @param file              The file to be uploaded to the server
 * @param url               The url of the ftp server
 * @param successCallback   The success callback
 * @param errorCallback     The error callback
 */
FtpClient.prototype.put = function(file, url, param , successCallback, errorCallback) {
	 return exec(successCallback, errorCallback, "FtpClient", "put", [file, url , param]);
};
    
/**
 * Download a file from a FTP server
 *
 * @param file              The file to be uploaded to the server
 * @param url               The url of the ftp server
 * @param successCallback   The success callback
 * @param errorCallback     The error callback
 */
FtpClient.prototype.get = function(file, url, param , successCallback, errorCallback) {
	 return exec(successCallback, errorCallback, "FtpClient", "get", [file, url , param]);
};
    
FtpClient.prototype.filelist = function(url, param , successCallback, errorCallback) {
	return exec(successCallback, errorCallback, "FtpClient", "filelist", ["", url , param]);
};

var ftpclient = new FtpClient();
module.exports = ftpclient;
